// Custom includes
#include "kinect2_bridge/Kinect2BridgeNodelet.hpp"

Kinect2BridgeNodelet::Kinect2BridgeNodelet() :
    Nodelet(),
    pKinect2Bridge(NULL)
{}

Kinect2BridgeNodelet::~Kinect2BridgeNodelet()
{
    if(pKinect2Bridge)
    {
        pKinect2Bridge->stop();
        delete pKinect2Bridge;
    }
}

void Kinect2BridgeNodelet::onInit()
{
    pKinect2Bridge = new Kinect2Bridge(getNodeHandle(), getPrivateNodeHandle());
    if(!pKinect2Bridge->start())
    {
        delete pKinect2Bridge;
        pKinect2Bridge = NULL;
        throw nodelet::Exception("Could not start kinect2_bridge!");
    }
}