#ifndef __KINECT2_BRIDGE_HPP__
#define __KINECT2_BRIDGE_HPP__

// STD includes
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#if defined(__linux__)
#include <sys/prctl.h>
#elif defined(__APPLE__)
#include <pthread.h>
#endif

// ROS includes
#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/Header.h>
#include <std_srvs/Empty.h>

// LibFreenect2 includes
#include <libfreenect2/config.h>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/packet_pipeline.h>
#include <libfreenect2/registration.h>

// OpenCV includes
#include <opencv2/opencv.hpp>

// Custom includes
#include <kinect2_bridge/kinect2_definitions.h>
#include <kinect2_registration/kinect2_registration.h>

class Kinect2Bridge
{
public:
    Kinect2Bridge(const ros::NodeHandle &nh = ros::NodeHandle(), const ros::NodeHandle &priv_nh = ros::NodeHandle("~"));
    bool start();
    void stop();

private:
    bool initialize();
    bool initRegistration(const std::string &method, const int32_t device, const double maxDepth);
    bool initPipeline(const std::string &method, const int32_t device);
    void initConfig(const bool bilateral_filter, const bool edge_aware_filter, const double minDepth, const double maxDepth);
    void initCompression(const int32_t jpegQuality, const int32_t pngLevel, const bool use_png);
    void initTopics(const int32_t queueSize, const std::string &base_name);
    bool initDevice(std::string &sensor);
    void initCalibration(const std::string &calib_path, const std::string &sensor);

    bool loadCalibrationFile(const std::string &filename, cv::Mat &cameraMatrix, cv::Mat &distortion) const;
    bool loadCalibrationPoseFile(const std::string &filename, cv::Mat &rotation, cv::Mat &translation) const;
    bool loadCalibrationDepthFile(const std::string &filename, double &depthShift) const;

    void createCameraInfo();
    void createCameraInfo(const cv::Size &size, const cv::Mat &cameraMatrix, const cv::Mat &distortion, const cv::Mat &rotation, const cv::Mat &projection, sensor_msgs::CameraInfo &cameraInfo) const;

    void callbackStatus();
    bool updateStatus(bool &isSubscribedColor, bool &isSubscribedDepth);
    void threadDispatcher(const size_t id);

    void receiveImages();
    bool receiveFrames(libfreenect2::SyncMultiFrameListener *listener, libfreenect2::FrameMap &frames);
    std_msgs::Header createHeader(ros::Time &last, ros::Time &other);

    void processIrDepth(const cv::Mat &depth, std::vector<cv::Mat> &images, const std::vector<Status> &status);
    void processColor(std::vector<cv::Mat> &images, const std::vector<Status> &status);

    void publishImages(const std::vector<cv::Mat> &images, const std_msgs::Header &header, const std::vector<Status> &status, const size_t frame, size_t &pubFrame, const size_t begin, const size_t end);
    void createImage(const cv::Mat &image, const std_msgs::Header &header, const Image type, sensor_msgs::Image &msgImage) const;
    void createCompressed(const cv::Mat &image, const std_msgs::Header &header, const Image type, sensor_msgs::CompressedImage &msgImage) const;
    void publishStaticTF();

    static inline void setThreadName(const std::string &name)
    {
#if defined(__linux__)
        prctl(PR_SET_NAME, name.c_str());
#elif defined(__APPLE__)
        pthread_setname_np(name.c_str());
#endif
    }

    bool onRequestImagesServiceCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);

    bool triggerMode;
    std::vector<int> compressionParams;
    std::string compression16BitExt, compression16BitString, baseNameTF;

    cv::Size sizeColor, sizeIr, sizeLowRes;
    libfreenect2::Frame color;
    cv::Mat cameraMatrixColor, distortionColor, cameraMatrixLowRes, cameraMatrixIr, distortionIr, cameraMatrixDepth, distortionDepth;
    cv::Mat rotation, translation;
    cv::Mat map1Color, map2Color, map1Ir, map2Ir, map1LowRes, map2LowRes;

    std::vector<std::thread> threads;
    std::mutex lockImgReceiver;
    std::mutex lockSync, lockPub, lockStatus;
    std::mutex lockRegLowRes, lockRegHighRes, lockRegSD;

    bool publishTF;
    std::thread tfPublisher;

    libfreenect2::Freenect2 freenect2;
    libfreenect2::Freenect2Device *device;
    libfreenect2::SyncMultiFrameListener *listener;
    libfreenect2::PacketPipeline *packetPipeline;
    libfreenect2::Registration *registration;
    libfreenect2::Freenect2Device::ColorCameraParams colorParams;
    libfreenect2::Freenect2Device::IrCameraParams irParams;

    ros::NodeHandle nh, priv_nh;

    DepthRegistration *depthRegLowRes, *depthRegHighRes;

    size_t frameCounter, pubFrame;
    ros::Time lastTimestamp;

    double deltaT;
    double depthShift;

    bool running, deviceActive, clientConnected, isSubscribedColor, isSubscribedDepth;

    // Publishers
    std::vector<ros::Publisher> imagePubs;
    std::vector<ros::Publisher> compressedPubs;
    ros::Publisher infoHDPub;
    ros::Publisher infoQHDPub;
    ros::Publisher infoIRPub;
    ros::ServiceServer requestImgsSrv;

    sensor_msgs::CameraInfo infoHD;
    sensor_msgs::CameraInfo infoQHD;
    sensor_msgs::CameraInfo infoIR;
    std::vector<Status> status;
};
#endif // __KINECT2_BRIDGE_HPP__