#ifndef __KINECT2_BRIDGE_NODELET_HPP__
#define __KINECT2_BRIDGE_NODELET_HPP__

// ROS includes
#include <nodelet/nodelet.h>

// Misc includes
#include <pluginlib/class_list_macros.h>

// Custom includes
#include "kinect2_bridge/Kinect2Bridge.hpp"

class Kinect2BridgeNodelet : public nodelet::Nodelet
{
public:
    Kinect2BridgeNodelet();
    ~Kinect2BridgeNodelet();

    virtual void onInit();

private:
    Kinect2Bridge *pKinect2Bridge;
};

PLUGINLIB_EXPORT_CLASS(Kinect2BridgeNodelet, nodelet::Nodelet)

#endif // __KINECT2_BRIDGE_NODELET_HPP__